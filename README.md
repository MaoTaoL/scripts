# Scripts
A repository for storing random scripts I wrote.

Icon by [Chris Ried](https://unsplash.com/@cdr6934?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/python-code?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText).